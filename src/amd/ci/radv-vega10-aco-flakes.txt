dEQP-VK.dynamic_rendering.basic.*
dEQP-VK.dynamic_rendering.primary.*

dEQP-VK.multiview.renderpass2.multisample.8_1_1_8
dEQP-VK.multiview.renderpass2.multisample.8
dEQP-VK.multiview.renderpass2.multisample.15_15_15_15

# https://gitlab.freedesktop.org/mesa/mesa/-/issues/8817
dEQP-VK.*framebuffer_attachment.diff_attachments.*
dEQP-VK.*framebuffer_attachment.multi_attachments_not_exported_2d.*
dEQP-VK.*framebuffer_attachment.resolve_input_same_attachment
